var WebApiWs=function() {
WebApiWs.initializeBase(this);
this._timeout = 0;
this._userContext = null;
this._succeeded = null;
this._failed = null;
}
WebApiWs.prototype={
_get_path:function() {
 var p = this.get_path();
 if (p) return p;
 else return WebApiWs._staticInstance.get_path();},
GetWebData:function(Method,JsonArgs,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetWebData',true,{Method:Method,JsonArgs:JsonArgs},succeededCallback,failedCallback,userContext); },
GetNewsList:function(PageId,NumberOfRecords,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetNewsList',true,{PageId:PageId,NumberOfRecords:NumberOfRecords},succeededCallback,failedCallback,userContext); },
GetBannerSlideShowList:function(BannerId,NumberOfRecords,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetBannerSlideShowList',true,{BannerId:BannerId,NumberOfRecords:NumberOfRecords},succeededCallback,failedCallback,userContext); }}
WebApiWs.registerClass('WebApiWs',Sys.Net.WebServiceProxy);
WebApiWs._staticInstance = new WebApiWs();
WebApiWs.set_path = function(value) { WebApiWs._staticInstance.set_path(value); }
WebApiWs.get_path = function() { return WebApiWs._staticInstance.get_path(); }
WebApiWs.set_timeout = function(value) { WebApiWs._staticInstance.set_timeout(value); }
WebApiWs.get_timeout = function() { return WebApiWs._staticInstance.get_timeout(); }
WebApiWs.set_defaultUserContext = function(value) { WebApiWs._staticInstance.set_defaultUserContext(value); }
WebApiWs.get_defaultUserContext = function() { return WebApiWs._staticInstance.get_defaultUserContext(); }
WebApiWs.set_defaultSucceededCallback = function(value) { WebApiWs._staticInstance.set_defaultSucceededCallback(value); }
WebApiWs.get_defaultSucceededCallback = function() { return WebApiWs._staticInstance.get_defaultSucceededCallback(); }
WebApiWs.set_defaultFailedCallback = function(value) { WebApiWs._staticInstance.set_defaultFailedCallback(value); }
WebApiWs.get_defaultFailedCallback = function() { return WebApiWs._staticInstance.get_defaultFailedCallback(); }
WebApiWs.set_enableJsonp = function(value) { WebApiWs._staticInstance.set_enableJsonp(value); }
WebApiWs.get_enableJsonp = function() { return WebApiWs._staticInstance.get_enableJsonp(); }
WebApiWs.set_jsonpCallbackParameter = function(value) { WebApiWs._staticInstance.set_jsonpCallbackParameter(value); }
WebApiWs.get_jsonpCallbackParameter = function() { return WebApiWs._staticInstance.get_jsonpCallbackParameter(); }
WebApiWs.set_path("/App_WsApi/WebApiWs.asmx");
WebApiWs.GetWebData= function(Method,JsonArgs,onSuccess,onFailed,userContext) {WebApiWs._staticInstance.GetWebData(Method,JsonArgs,onSuccess,onFailed,userContext); }
WebApiWs.GetNewsList= function(PageId,NumberOfRecords,onSuccess,onFailed,userContext) {WebApiWs._staticInstance.GetNewsList(PageId,NumberOfRecords,onSuccess,onFailed,userContext); }
WebApiWs.GetBannerSlideShowList= function(BannerId,NumberOfRecords,onSuccess,onFailed,userContext) {WebApiWs._staticInstance.GetBannerSlideShowList(BannerId,NumberOfRecords,onSuccess,onFailed,userContext); }
