var WebWs=function(){WebWs.initializeBase(this);this._timeout=0;this._userContext=null;this._succeeded=null;this._failed=null;}
WebWs.prototype={_get_path:function(){var p=this.get_path();if(p)return p;else return WebWs._staticInstance.get_path();},HelloWorld:function(succeededCallback,failedCallback,userContext){return this._invoke(this._get_path(),'HelloWorld',false,{},succeededCallback,failedCallback,userContext);},GetTotalItemInShoppingCart:function(succeededCallback,failedCallback,userContext){return this._invoke(this._get_path(),'GetTotalItemInShoppingCart',false,{},succeededCallback,failedCallback,userContext);},WebSelectLanguage:function(strLanguageCode,succeededCallback,failedCallback,userContext){return this._invoke(this._get_path(),'WebSelectLanguage',false,{strLanguageCode:strLanguageCode},succeededCallback,failedCallback,userContext);},AddWishListItemToCart:function(PageID,ProductCategoryID,ProductVariationID,Quantity,ProductGUID,WishListItemGUID,MemberGUID,CompanyGUID,succeededCallback,failedCallback,userContext){return this._invoke(this._get_path(),'AddWishListItemToCart',false,{PageID:PageID,ProductCategoryID:ProductCategoryID,ProductVariationID:ProductVariationID,Quantity:Quantity,ProductGUID:ProductGUID,WishListItemGUID:WishListItemGUID,MemberGUID:MemberGUID,CompanyGUID:CompanyGUID},succeededCallback,failedCallback,userContext);}}
WebWs.registerClass('WebWs',Sys.Net.WebServiceProxy);WebWs._staticInstance=new WebWs();WebWs.set_path=function(value){WebWs._staticInstance.set_path(value);}
WebWs.get_path=function(){return WebWs._staticInstance.get_path();}
WebWs.set_timeout=function(value){WebWs._staticInstance.set_timeout(value);}
WebWs.get_timeout=function(){return WebWs._staticInstance.get_timeout();}
WebWs.set_defaultUserContext=function(value){WebWs._staticInstance.set_defaultUserContext(value);}
WebWs.get_defaultUserContext=function(){return WebWs._staticInstance.get_defaultUserContext();}
WebWs.set_defaultSucceededCallback=function(value){WebWs._staticInstance.set_defaultSucceededCallback(value);}
WebWs.get_defaultSucceededCallback=function(){return WebWs._staticInstance.get_defaultSucceededCallback();}
WebWs.set_defaultFailedCallback=function(value){WebWs._staticInstance.set_defaultFailedCallback(value);}
WebWs.get_defaultFailedCallback=function(){return WebWs._staticInstance.get_defaultFailedCallback();}
WebWs.set_enableJsonp=function(value){WebWs._staticInstance.set_enableJsonp(value);}
WebWs.get_enableJsonp=function(){return WebWs._staticInstance.get_enableJsonp();}
WebWs.set_jsonpCallbackParameter=function(value){WebWs._staticInstance.set_jsonpCallbackParameter(value);}
WebWs.get_jsonpCallbackParameter=function(){return WebWs._staticInstance.get_jsonpCallbackParameter();}
WebWs.set_path("/App_Ws/WebWs.asmx");WebWs.HelloWorld=function(onSuccess,onFailed,userContext){WebWs._staticInstance.HelloWorld(onSuccess,onFailed,userContext);}
WebWs.GetTotalItemInShoppingCart=function(onSuccess,onFailed,userContext){WebWs._staticInstance.GetTotalItemInShoppingCart(onSuccess,onFailed,userContext);}
WebWs.WebSelectLanguage=function(strLanguageCode,onSuccess,onFailed,userContext){WebWs._staticInstance.WebSelectLanguage(strLanguageCode,onSuccess,onFailed,userContext);}
WebWs.AddWishListItemToCart=function(PageID,ProductCategoryID,ProductVariationID,Quantity,ProductGUID,WishListItemGUID,MemberGUID,CompanyGUID,onSuccess,onFailed,userContext){WebWs._staticInstance.AddWishListItemToCart(PageID,ProductCategoryID,ProductVariationID,Quantity,ProductGUID,WishListItemGUID,MemberGUID,CompanyGUID,onSuccess,onFailed,userContext);}