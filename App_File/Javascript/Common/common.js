var monthFullNames=["January","February","March","April","May","June","July","August","September","October","November","December"];var monthShortNames=["Jan","Feb","Mar","Apr","May","Jun","July","Aug","Sep","Oct","Nov","Dec"];function getDayWithSuffix(dayNumber,withSuffixEffect){switch(parseInt(dayNumber).toString().slice(-1)){case '1':if(withSuffixEffect==true)
return '1<sup>st</sup>';else
return '1st';break;case '2':if(withSuffixEffect==true)
return '2<sup>nd</sup>';else
return '2nd';break;case '3':if(withSuffixEffect==true)
return '3<sup>rd</sup>';else
return '3rd';break;default:if(withSuffixEffect==true){return parseInt(dayNumber).toString()+'<sup>th</sup>';}else{return parseInt(dayNumber).toString()+'th';}
break;}}